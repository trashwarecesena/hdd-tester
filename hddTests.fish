#!/bin/fish

#
# Variabili globali
#

set disks;

#
# Funzioni
#

# Funzione che crea il prompt per read come si desidera
function readPrompt
  printf 'set_color yellow --bold; echo -n "%s"; set_color normal; echo;' $argv;
end

#
# Script
#

#
# Validazione input
#

# Controllo che i parametri di input siano dispositivi a blocchi
# e tolgo quelli che non lo sono
for argumentIndex in (seq (count $argv))
  if not test -b $argv[$argumentIndex]
    set_color red;
    echo '"'$argv[$argumentIndex]'" non è un dispositivo a blocchi.';
    set_color normal;
  else
    set disks $disks $argv[$argumentIndex];
  end
end
echo;

# Se nessun parametro è giusto, scrivi delle informazioni di utilizzo dello script ed esci
if test (count $disks) -eq 0
  echo 'hddTests - Script per testare HDD secondo i fantomatici standard Trashware.';
  echo;
  echo 'Usage:  ./hddTests.fish /dev/sdX /dev/sdY/ ...'
  echo;
  exit;
end

# Mostra informazioni generali sul disco ed abilita lo S.M.A.R.T., se necessario.
for disk in $disks
  set_color green --bold;
  echo 'Infommazzzioni su "'$disk'":';
  echo;
  set_color normal;
  sudo smartctl -i $disk;
  read -p (readPrompt 'Lo S.M.A.R.T. è disabilitato e può essere abilitato? [y/N] ') answer;
  echo;
  if test $answer = 'y'
    sudo smartctl -s on $disk;
  end
  read -p (readPrompt 'Annota il nome "'$disk'" sul relativo disco fisico e premi [Invio]...');
  echo;
end

# Effettua uno S.M.A.R.T. short test, se necessario.
set testedDisks;
for disk in $disks
  set_color green --bold;
  echo 'Salute di "'$disk'":';
  echo;
  set_color normal;
  sudo smartctl -H $disk;
  sudo smartctl -A $disk;
  read -p (readPrompt 'Effettuare uno S.M.A.R.T. short test? [Y/n] ') answer;
  echo;
  if not test $answer = 'n'
    set testedDisks $testedDisks $disk;
    sudo smartctl -t short $disk;
    echo;
  end
  read -p (readPrompt 'Passa al prossimo disco premendo [Invio]...');
  echo;
end

if not test (count $testedDisks) -eq 0
  read -p (readPrompt 'Ora attendi che i test terminino (ogni test indica a che ora finisce) e premi [Invio]...');
  echo;
  for disk in $testedDisks
    set_color green --bold;
    echo 'Risultati test su "'$disk'":';
    set_color normal;
    sudo smartctl -l selftest $disk;
    read -p (readPrompt 'Passa al prossimo disco premendo [Invio]...');
    echo;
  end
end

set oldDisksList $disks;
set disks;
set_color magenta;
echo 'Scegli su quali dischi vuoi proseguire la procedura:';
for disk in $oldDisksList
  read -p (readPrompt '- vuoi mantenere '$disk'? [Y/n] ') answer;
  if not test $answer = 'n'
    set disks $disks $disk;
  end;
end
echo;
if test (count $disks) -eq 0
  set_color red;
  echo 'Non ci sono più dischi su cui lavorare :(`';
  exit;
else
  set_color magenta;
  echo 'Continuo a lavorare su:'
  for disk in $disks
    echo '- '$disk;
  end
  echo;
end
set_color normal;

# Controllo la velocità di lettura di ogni disco
set_color magenta;
echo "Ora eseguirò dei test di lettura, uno ogni 20GiB partendo dall'inizio del disco.";
echo "La velocità dovrebbe scendere gradualmente dal primo all'ultimo test,";
echo "fino ad essere la metà della velocità del primo test.";
echo -n 'Se la velocità varia di molto tra i vari test o scende a più della metà del primo test, ';
set_color magenta --bold;
echo -n 'il disco è rotto'
set_color normal;
set_color magenta;
echo '.';
echo;
set_color normal;

for disk in $disks
  set_color green --bold;
  echo 'Velocità di lettura di "'$disk'":';
  set_color normal;
  set offset -20;
  set hdparmStatus 0;
  while test $hdparmStatus -eq 0
    set offset (math $offset+20);
    sudo hdparm -t --offset $offset $disk;
    set hdparmStatus $status;
  end
  echo;
  read -p (readPrompt 'Passa al prossimo disco premendo [Invio]...');
  echo;
end

# Controllo la velocità di scrittura di ogni disco
set_color magenta;
echo 'Ora eseguirò alcuni test di scrittura, sempre a cadenze di 20GiB come quelli di lettura.';
echo 'I test sono eseguiti bypassando il sistema di caching del S.O., ma utilizzando quello del disco.'
echo;
set_color normal;

for disk in $disks
  set_color green --bold;
  echo 'Velocità di scrittura di "'$disk'":';
  echo;
  set_color normal;
  set offset -20;
  set ddStatus 0;
  while test $ddStatus -eq 0
    set offset (math $offset+20);
    sudo dd if=/dev/zero of=$disk bs=512M seek=$offset count=1 oflag=direct
    set ddStatus $status;
    echo;
  end
  read -p (readPrompt 'Passa al prossimo disco premendo [Invio]...');
  echo;
end

# Test del seektime medio
set_color magenta;
echo 'Ora eseguirò dei test sulla velocità di seek media dei dischi.'
echo;
set_color normal;

for disk in $disks
  set_color green --bold;
  echo 'Velocità di seek media di "'$disk'":';
  echo;
  set_color normal;
  sudo ./seeker $disk
  echo;
end

# Controllo veloce sulle temperature dei dischi, dopo un po' di esercizio
set_color green --bold;
echo 'Temperature dei dischi:';
set_color normal;

for disk in $disks
  echo -n '- ';
  sudo hddtemp $disk;
end
echo;
